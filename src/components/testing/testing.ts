import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the TestingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'testing',
  templateUrl: 'testing.html'
})
export class TestingComponent {
  @Input('myText') textToUse;
  @Output() somethingHappened = new EventEmitter;

  text: string; 

  constructor() {
    console.log('Hello LoginFormComponent Component');
    this.text = "Default Text"
  }

  ngAfterViewInit(){
    if(this.textToUse){this.text = this.textToUse;}
    setInterval  //let interval = 
    (
      ()=>{
      this.somethingHappened.emit("it's time");
    }
      ,3000
    )
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QrCodeStuffPage } from './qr-code-stuff';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  declarations: [
   QrCodeStuffPage,
  ],
  imports: [
    IonicPageModule.forChild(QrCodeStuffPage),
    NgxQRCodeModule
    
  ],
})
export class QrCodeStuffPageModule {}

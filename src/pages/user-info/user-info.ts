import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import * as moment from 'moment';

/**
 * Generated class for the UserInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-info',
  templateUrl: 'user-info.html',
})
export class UserInfoPage {
  private user = null;
  private birthday = null;
  private picClass = "noRotate";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {
    this.user = navParams.get('userID')
    this.birthday = moment(this.user.birthday).add(-1, "M").format("MMMM DD")

    //the following rotation magic is from this site: https://stackoverflow.com/questions/48493176/css-transform-image-rotate-overlapping-other-divs?rq=1&utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    if(this.user.rotate == "rotate(90deg)"){
      this.picClass = "rotate90"
    } 
    if(this.user.rotate == "rotate(-90deg)"){
      this.picClass = "rotateNeg90"
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
  }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

}

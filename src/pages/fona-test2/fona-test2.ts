import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
import * as Fuse from "fuse.js";
import _ from "lodash";
import { ModalController } from 'ionic-angular';
import { LoadingController } from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the FonaTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fona-test2',
  templateUrl: 'fona-test2.html',
})
export class FonaTest2Page {
  private resultsArray = null;
  private backupArray = null;

  // variables for search
  private filteredSearchItems = null;
  private searchItems = null;
  private searching = null;
  private searchInput = null;
  private addedSearches = [];
  private searchingBackup = null;
  private menu = false;
  private inputClicked = 0;
  private admin = false;

  //hard inputs
  private selectedForm = "Any";
  private selectedProfile = "Any";
  private phys_forms = ["Dry", "Liquid/ OS", "Liquid/ Neat", "Liquid/ WS", "Any"]
  private profiles = ["Any","Berry", "Brown","Citrus","Dairy", "Melon", "Mint", "Orchard", "Other", "Sensate", "Spice", "Tolling", "Tropical"]

  private debug = false;

  private loading = null;
  private database = "/flavors/"

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private auth: AuthenticationProvider
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FonaTestPage');
    this.loading = this.loadingCtrl.create({
      spinner: "ios",
      content: "Loading Flavors..."
    });
    this.loading.present();
    if (this.auth.currentUserData.permissions === "administrator") {
      this.admin = true;
      this.database = "/flavorings/"
    }
    this.getData();
    this.loading.dismiss();
  }

  getData() {
    firebase
      .database()
      .ref(this.database)
      .once("value")
      .then(snapshot => {
        //.orderByChild('displayName');
        this.resultsArray = snapshot.val();
        this.backupArray = this.resultsArray;
        console.log(this.resultsArray)
      }); //end firebase.database
  }

  selectForm(event) {
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["FORM"])
      this.addedSearches.push("form:" + event)
      if(this.resultsArray.length < 2){
        this.menu = false;
      }
      this.addPlaceholder();
    }

    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        let tag = temp[1];
        if (type === "form") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("form:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          //this.selectedForm = tag;
          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("form:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    }

    this.inputClicked = 0;

  }

  selectProfile(event) {
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["FLAVOR PROFILE"])
      this.addedSearches.push("prof:" + event)
      this.addPlaceholder();
    }

    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        let tag = temp[1];
        if (type === "prof") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("prof:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          //this.selectedForm = tag;
          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("prof:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
        
      }
    }
    this.inputClicked = 0;
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    //this.initializeItems();
    if (this.debug) {
      console.log("searchItems", this.searchItems);
      console.log("this.resultsArray", this.resultsArray);
    }

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (this.debug) {
      console.log("ev", ev);
      console.log("val", val);
      console.log(ev.data);
    }
    if (ev.data == null) {
      // if this is null, then the user hit 'backspace' to kill everything
      //this.updatelist;   // so reset the filters to show the restaurants again.
    }

    if (val == null) {
      //this.reset();
    }

    if (val != null && val.length < 1 && this.searching == true) {
      //this case is invoked if we were searching, but we deleted to start again.
      this.searching = false; //this.searching tells the html that we are actively searching for stuff, so it changes what is displayed.
      //this.getData();
    }

    if(val != null && val.length == 0){
      this.resultsArray = this.searchingBackup;
    }
    if(val == null){
      this.resultsArray = this.searchingBackup;
    }

    if (val != null && val.length > 0) {
      this.resultsArray = this.fuseSearch(val, this.searchingBackup, ["id", "DESCRIPTORS"]); //Search it!!!
      if(this.resultsArray.length < 2){
        this.menu = false;
      }
    }
  }

  saveInputResults(){
    if(this.inputClicked<1){
    this.searchingBackup = this.resultsArray;
    this.inputClicked++;
    }
  }

  reset() {
    this.resultsArray = this.backupArray;
    this.addedSearches = [];
    this.searchInput = null;
    this.selectedForm = "Any";
    this.selectedProfile = "Any";
    this.inputClicked = 0;
  }


  fuseSearch(searchTerm, arrayToBeSearched, keys) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: keys  ///////////////////////////////
    };

    if (this.debug) {
      console.log("fuzzywuzzy", searchTerm);
    }
    var fuse = new Fuse(arrayToBeSearched, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchTerm);
    if (this.debug) {
      console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      el.item.matches = el.matches; // the matches matrix shows what actually matched during the search.  We're storing it inside of filteredSearchItems
      var matchedFoodArray = _.map(el.matches, function (le) {
        if (le.key == "menu_items.name") {
          return el.item.menu_items[le.arrayIndex];
        } //Here's we're using the array index for the matching items to look up the actual menu items.
      });
      if (matchedFoodArray[0] != undefined) {
        el.item.matching_food = matchedFoodArray; //the matching_food array is the one that will be displayed on the search screen results.
      }
      return el.item;
    });

    if (this.debug) {
      console.log("final array", arr);
    }
    return arr;
  } // end of fuse search

  showInfo($event, flavor) {
    this.presentFlavorModal(flavor);
    event.stopPropagation();
  }

  presentFlavorModal(user) {
    const contactModal = this.modalCtrl.create("FlavorInfoPage", { flavorID: user });
    contactModal.present();
  }

  addSearch() {
    this.addedSearches.push("desc:" + this.searchInput)
    this.searchInput = null;
    this.inputClicked = 0;
  }

  eraseTag(tag, fromTag) {
    let temp = tag.split(":");
    let type = temp[0]
    let tagname = temp[1]
    if (fromTag) {
      if (type === "form") {
        //this.selectForm("Any");
        this.selectedForm = "Any";
      }

      if (type === "prof") {
        //this.selectProfile("Any");
        this.selectedProfile="Any";
      }
    }
    var index = this.addedSearches.indexOf(tag);
    if (index > -1) {
      this.addedSearches.splice(index, 1);
    }
    if (this.addedSearches.length > 0) {
      this.arraySearch(this.addedSearches, this.backupArray);
    } else {
      this.resultsArray = this.backupArray;
    }

  }

  arraySearch(searchArray, arrayToBeSearched) {
    if (searchArray.length == 0) {
      this.reset();
    } else {
      var tempArray = []
      for (let i = 0; i <= searchArray.length - 1; i++) {
        //first let's split up the tag so that we can do the right search
        let tag = searchArray[i].split(":");
        let type = tag[0];
        let value = tag[1];

        if (type == "desc") {
          console.log("desc");
          console.log(searchArray)
          console.log(arrayToBeSearched);
          console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["id", "DESCRIPTORS"])
          console.log(tempArray);
        }

        if (type == "form" && value != "Any") {
          console.log("form");
          console.log(searchArray)
          console.log(arrayToBeSearched);
          console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["FORM"])
          console.log(tempArray);
        }

        if (type == "prof" && value != "Any") {
          console.log("form");
          console.log(searchArray)
          console.log(arrayToBeSearched);
          console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["FLAVOR PROFILE"])
          console.log(tempArray);
        }


        arrayToBeSearched = tempArray;
      }
      this.resultsArray = tempArray;
      this.addPlaceholder();
      if(this.resultsArray.length < 2){
        this.menu = false;
      }
    }
  }

  toggleMenu() {
    this.menu = !this.menu;
    this.addPlaceholder();
    if(!this.menu){
      this.resultsArray.shift()
    }
  }

  addPlaceholder(){
    if(this.menu){
    let temp = JSON.parse(JSON.stringify(this.resultsArray[0]));
    temp["FLAVOR PROFILE"] = "White"
    this.resultsArray.unshift(temp)
    }
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FonaTest2Page } from './fona-test2';

@NgModule({
  declarations: [
    FonaTest2Page,
  ],
  imports: [
    IonicPageModule.forChild(FonaTest2Page),
  ],
})
export class FonaTest2PageModule {}

import {
  Component,
  trigger,
  transition,
  style,
  state,
  animate,
  keyframes
} from "@angular/core";
import { IonicPage, NavController } from "ionic-angular"; // NavParams
//import { LandingPage } from "../landing/landing";
import { ViewChild } from "@angular/core";
import { Slides } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AlertController } from 'ionic-angular';

// https://coursetro.com/posts/code/76/Create-an-Animated-App-Onboarding-Process-with-Ionic-3

@IonicPage()
@Component({
  selector: "page-slideshow",
  templateUrl: "slideshow.html",
  animations: [
    trigger("bounce", [
      state(
        "*",
        style({
          transform: "translateX(0)"
        })
      ),
      transition(
        "* => rightSwipe",
        animate(
          "700ms ease-out",
          keyframes([
            style({ transform: "translateX(0)", offset: 0 }),
            style({ transform: "translateX(-130px)", offset: 0.3 }),
            style({ transform: "translateX(0)", offset: 1.0 })
          ])
        )
      ),
      transition(
        "* => leftSwipe",
        animate(
          "700ms ease-out",
          keyframes([
            style({ transform: "translateX(0)", offset: 0 }),
            style({ transform: "translateX(130px)", offset: 0.3 }),
            style({ transform: "translateX(0)", offset: 1.0 })
          ])
        )
      )
    ])
  ]
})
export class SlideshowPage {
  @ViewChild(Slides) slides: Slides;
  skipMsg: string = "Skip";
  state: string = "x";
  private showNeverShowBtn = false;

  constructor(
    private navCtrl: NavController,
    //private navParams: NavParams,
    private storage: Storage,
    private screenOrientation: ScreenOrientation,
    public alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad SlideshowPage");
    //this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidLeave(){
    //this.screenOrientation.unlock();
    // Get current orientation
    console.log(this.screenOrientation.type);
  }

  skip() {
    this.presentConfirm()
  }

  slideChanged() {
    if (this.slides.isEnd()) {
      this.skipMsg = "Alright, I got it!";
      this.showNeverShowBtn = true;
    }
  }

  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex())
      this.state = "rightSwipe";
    else this.state = "leftSwipe";
  }

  animationDone() {
    this.state = "x";
    this.showNeverShowBtn=true;
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Stop showing tutorial?',
      message: 'Would you like to skip this tutorial next time?',
      buttons: [
        {
          text: 'Nope',
          handler: () => {
            this.storage.set("show_slideshow", true);
            this.navCtrl.setRoot("LandingPage");
          }
        },
        {
          text: 'Yep',
          role: 'cancel',
          handler: () => {
            this.storage.set("show_slideshow", false);
            this.navCtrl.setRoot("LandingPage");
          }
        }
      ]
    });
    alert.present();
  }


}

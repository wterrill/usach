import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestrictVideoPage } from './restrict-video';

@NgModule({
  declarations: [
    RestrictVideoPage,
  ],
  imports: [
    IonicPageModule.forChild(RestrictVideoPage),
  ],
})
export class RestrictVideoPageModule {}

import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as firebase from "firebase";
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the RestrictVideoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-restrict-video",
  templateUrl: "restrict-video.html"
})
export class RestrictVideoPage {
  private toggles = [];

  private target = null;
  private permissions = null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private toastCtrl: ToastController
  ) {
    this.target = this.navParams.data.target;
    if (this.target.length === 1) {
      if (this.navParams.data.target[0].videoPermissions !== undefined) {
        this.permissions = this.navParams.data.target[0].videoPermissions.array; 
        for (var i = 0; i < this.permissions.length; i++) {
          if (this.permissions[i] != 0) {
            this.toggles[i] = true;
          } else {
            this.toggles[i] = false;
          }
        }
      }
    }

    console.log(this.target);
    console.log(this.toggles);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad RestrictVideoPage");
  }

  restrictIt() {
    var array = [];

    if (this.toggles[0]) {
      array.push("1stRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[1]) {
      array.push("2ndRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[2]) {
      array.push("3rdRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[3]) {
      array.push("4thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[4]) {
      array.push("5thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[5]) {
      array.push("6thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[6]) {
      array.push("7thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[7]) {
      array.push("8thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[8]) {
      array.push("9thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[9]) {
      array.push("10thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[10]) {
      array.push("11thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[11]) {
      array.push("12thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[12]) {
      array.push("13thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[13]) {
      array.push("14thRoutine");
    } else {
      array.push(0);
    }

    if (this.toggles[14]) {
      array.push("15thRoutine");
    } else {
      array.push(0);
    }

    for (var i = 0; i < this.target.length; i++){
      firebase
        .database()
        .ref("users/" + this.target[i].uid + "/videoPermissions/")
        .set({
          array
        });
      this.presentToast(this.target[i]);
      }
  }

  presentToast(user) {
    let toast = this.toastCtrl.create({
      message: 'Routines updated for user: ' + user.displayName,
      duration: 1000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}

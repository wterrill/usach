import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FonaTestPage } from './fona-test';

@NgModule({
  declarations: [
    FonaTestPage,
  ],
  imports: [
    IonicPageModule.forChild(FonaTestPage),
  ],
})
export class FonaTestPageModule {}

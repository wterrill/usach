import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
import * as Fuse from "fuse.js";
import _ from "lodash";
import { ModalController } from 'ionic-angular';
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the FonaTestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fona-test',
  templateUrl: 'fona-test.html',
})
export class FonaTestPage {
  private resultsArray = null;
  private backupArray = null;

  // variables for search
  private filteredSearchItems = null;
  private searchItems = null;
  private searching = null;
  private searchInput = null;
  private addedSearches = [];
  private database = "/flavors/"
  private admin = false;

  private debug = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private auth: AuthenticationProvider
  ) {
    //this.getData();
    this.resultsArray = this.navParams.get("resultsArray")
    this.backupArray = this.resultsArray;
    if (this.auth.currentUserData.permissions === "administrator") {
      this.admin = true;
      this.database = "/flavorings/"
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FonaTestPage');
  }

  // getData(){
  //   firebase
  //       .database()
  //       .ref("/flavors/")
  //       .once("value")    
  //       .then(snapshot => {
  //         //.orderByChild('displayName');
  //         this.resultsArray = snapshot.val();
  //         this.backupArray = this.resultsArray;
  //         console.log(this.resultsArray)
  //       }); //end firebase.database
  // }

  getItems(ev: any) {
    // Reset items back to all of the items
    //this.initializeItems();
    if (this.debug) {
      console.log("searchItems", this.searchItems);
      console.log("this.resultsArray", this.resultsArray);
    }

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (this.debug) {
      console.log("ev", ev);
      console.log("val", val);
      console.log(ev.data);
    }
    if (ev.data == null) {
      // if this is null, then the user hit 'backspace' to kill everything
      //this.updatelist;   // so reset the filters to show the restaurants again.
    }

    if (val == null) {
      //this.reset();
    }

    if (val != null && val.length < 1 && this.searching == true) {
      //this case is invoked if we were searching, but we deleted to start again.
      this.searching = false; //this.searching tells the html that we are actively searching for stuff, so it changes what is displayed.
      //this.getData();
    }

    if (val != null && val.length > 0) {
      //don't start search unless we have at least three characters.
      this.searching = true; //this shows that we're searching and changes what is seen in the html
      this.resultsArray = this.fuseSearch(val, this.resultsArray); //Search it!!!
      //this.gridDisplay = false; //Clear it!!!
      //this.setupDisplay(this.filteredSearchItems); //Display it!!!
    }
  }

  reset() {
    this.resultsArray = this.backupArray;
    this.addedSearches = [];
    this.searchInput = null;
  }


  fuseSearch(searchTerm, arrayToBeSearched) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: ["id", "DESCRIPTORS"]  ///////////////////////////////
    };

    if (this.debug) {
      console.log("fuzzywuzzy", searchTerm);
    }
    var fuse = new Fuse(arrayToBeSearched, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchTerm);
    if (this.debug) {
      console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      el.item.matches = el.matches; // the matches matrix shows what actually matched during the search.  We're storing it inside of filteredSearchItems
      var matchedFoodArray = _.map(el.matches, function (le) {
        if (le.key == "menu_items.name") {
          return el.item.menu_items[le.arrayIndex];
        } //Here's we're using the array index for the matching items to look up the actual menu items.
      });
      if (matchedFoodArray[0] != undefined) {
        el.item.matching_food = matchedFoodArray; //the matching_food array is the one that will be displayed on the search screen results.
      }
      return el.item;
    });

    if (this.debug) {
      console.log("final array", arr);
    }
    return arr;
  } // end of fuse search

  showInfo($event, flavor) {
    this.presentFlavorModal(flavor);
    event.stopPropagation();
  }

  presentFlavorModal(user) {
    const contactModal = this.modalCtrl.create("FlavorInfoPage", { flavorID: user });
    contactModal.present();
  }

  addSearch() {
    this.addedSearches.push(this.searchInput)
    this.searchInput = null;
  }

  eraseTag(tag) {
    var index = this.addedSearches.indexOf(tag);
    if (index > -1) {
      this.addedSearches.splice(index, 1);
    }
    this.arraySearch(this.addedSearches, this.backupArray);
  }

  arraySearch(searchArray, arrayToBeSearched) {
    if (searchArray.length == 0) {
      this.reset();
    } else {
      var tempArray = []
      for (let i = 0; i <= searchArray.length - 1; i++) {
        console.log(searchArray)
        console.log(arrayToBeSearched);
        console.log(tempArray);

        tempArray = this.fuseSearch(searchArray[i], arrayToBeSearched)
        console.log(tempArray);

        arrayToBeSearched = tempArray;
      }
      this.resultsArray = tempArray;
    }
  }
}

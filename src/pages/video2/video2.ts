import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

/**
 * Generated class for the Video2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


//  https://github.com/ihadeed/CordovaYoutubeVideoPlayer


@IonicPage()
@Component({
  selector: 'page-video2',
  templateUrl: 'video2.html',
})
export class Video2Page {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private youtube: YoutubeVideoPlayer
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Video2Page');
    this.youtube.openVideo('1kWQT9oAdP4');
  }

}

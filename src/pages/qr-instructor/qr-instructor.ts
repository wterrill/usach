import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular'; //NavController, NavParams 
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import * as moment from "moment";

/**
 * Generated class for the QrInstructorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qr-instructor',
  templateUrl: 'qr-instructor.html',
})
export class QrInstructorPage {
  private scannedCode: any = null;
  private studentPhoto = null;
  private displayName = null;
  private email = null;
  private uid = null;
  private amount = null;
  private date = null;
  //private paidUntil = null;
  //private cost = null;
  private result = null;
  private result2 = null;
  private account = 10000;
  private rut = null;
  private tipo_cuenta = null;
  private cuenta_Num = null;
  private banco = null;
  private recipient = null;

  // "username": this.username.displayName,
  // "uid":this.username.uid,
  // "photo":this.username.photoURL,
  // "email":this.username.email, 
  // "amount": this.amount, 
  // "date": this.date, 
  // "paidUntil": this.period, 
  // "cost": this.cost


  constructor(
    // private navCtrl: NavController, 
    // private navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    private alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QrInstructorPage');
  }

  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      //alert(JSON.stringify(barcodeData)); 
      // if(barcodeData.cancelled)
      // {alert("cancelled")}
      if (!barcodeData.cancelled) {
        this.presentReceipt(JSON.parse(barcodeData.text))
        this.scannedCode = barcodeData.text;
        this.displayInfo();
      }
    }).catch(err => {
    });
  }



  displayInfo() {

    this.result = JSON.parse(this.scannedCode);
    //alert(JSON.stringify(this.result))
    this.studentPhoto = this.result.photo;
    this.displayName = this.result.displayName;
    this.email = this.result.email;
    this.uid = this.result.uid;
    this.rut = this.result.rut,
    this.tipo_cuenta = this.result.tipo_cuenta,
    this.cuenta_Num = this.result.cuenta_Num,
    this.banco = this.result.banco,
    this.date = this.result.date;
  }


  presentReceipt(object) {
    //alert(JSON.stringify(object))
    //alert("<img height='42' src='"+ object.photo +"' />")
    let alertx = this.alertCtrl.create({
      title: 'Favor, confirmar la persona',
      message: "<img class = 'image' src='"+ object.photo +"' ><p>" + object.displayName + " es la persona que quieres pagar, cierto?</p>",
      buttons: [
        {
          text: 'SI',
          role: 'cancel',
          handler: () => {
            console.log("cancelled");
            this.recipient = object
            //this.result2 = true;
          }
        },
        {
          text: 'NO',
          handler: () => {
            this.scanCode()
          }
        }
      ],
      cssClass: "alertcss"
    });
    alertx.present();
  }

  sendIt() {
    if (this.amount != null) {

      let alertx = this.alertCtrl.create({
        title: 'Pago Confirmado!',
        message: "<p> &nbsp; </p><p>" + this.recipient.displayName + " ha recibido " + this.amount + " pesos. </p> <p>Gracias por usar Sencillo!</P>",
        buttons: [
          {
            text: 'Hecho',
            //role: 'cancel',
            handler: () => {
              this.account = this.account - this.amount
            }
          }
        ],
        cssClass: "alertcss"
      });
      alertx.present();
    } else {
      let alertx = this.alertCtrl.create({
        title: 'Error!',
        message: "<p>Tienes que ingresar un monto para completar la transaccion</p>",
        buttons: [
          {
            text: 'Intenta de nuevo',
            role: 'cancel',
          }
        ],
        cssClass: "alertcss"
      });
      alertx.present();
    }
  }


}//end of class

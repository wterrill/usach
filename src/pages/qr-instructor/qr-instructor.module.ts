import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QrInstructorPage } from './qr-instructor';

@NgModule({
  declarations: [
    QrInstructorPage,
  ],
  imports: [
    IonicPageModule.forChild(QrInstructorPage),
  ],
})
export class QrInstructorPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VideoPlayer, VideoOptions } from '@ionic-native/video-player';

/**
 * Generated class for the Video3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 

 // https://ionicframework.com/docs/native/video-player/
 //  https://github.com/moust/cordova-plugin-videoplayer


@IonicPage()
@Component({
  selector: 'page-video3',
  templateUrl: 'video3.html',
})
export class Video3Page {
  videoOptions: VideoOptions;
  videoURL: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private videoPlayer: VideoPlayer
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Video3Page');



  }

  async playVideo(){
    try{
      this.videoOptions ={
        volume: 0.7
      }


      this.videoURL = "http://techslides.com/demos/sample-videos/small.mp4"  //   "https://www.youtube.com/embed/wyVM1evRxNw"
      this.videoPlayer.play(this.videoURL,this.videoOptions).then(() => {
       console.log('video completed');
      }).catch(err => {
       console.log(err);
      });





    }

    catch(e){
      console.error(e);
    }
  }

}

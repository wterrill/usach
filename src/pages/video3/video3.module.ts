import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Video3Page } from './video3';

@NgModule({
  declarations: [
    Video3Page,
  ],
  imports: [
    IonicPageModule.forChild(Video3Page),
  ],
})
export class Video3PageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
import * as Fuse from "fuse.js";
import _ from "lodash";
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the SelectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select',
  templateUrl: 'select.html',
})
export class SelectPage {
  private resultsArray = null;
  private backupArray = null;

  private selectedForm = null;
  private selectedProfile = null;
  private phys_forms = ["Dry","Liquid/ OS","Liquid/ Neat", "Liquid/ WS","Any"]
  private profiles = ["Berry","Melon","Sensate","Brown","Mint","Spice","Citrus","Orchard","Tolling","Dairy","Other","Tropical", "Any"]
  

  private debug = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectPage');
  }

  getData(){
    firebase
        .database()
        .ref("/flavors/")
        .once("value")    
        .then(snapshot => {
          //.orderByChild('displayName');
          this.resultsArray = snapshot.val();
          this.backupArray = this.resultsArray;
          console.log(this.resultsArray)
        }); //end firebase.database
  }

  selectForm(event){
    
    //this.resultsArray = this.backupArray;
    if(event !== "Any"){
    this.resultsArray = this.fuseSearch(event,["FORM"])
    }

    if(event !== "Any"){
      this.resultsArray = this.fuseSearch(event,["FORM"])
    } else
    if ((this.selectedProfile === "Any" || this.selectedProfile === null) && event === "Any" ){
      this.resultsArray = this.backupArray;
    } else
    if ((this.selectedProfile !== "Any" || this.selectedProfile !== null) && event === "Any" ){
      this.selectForm(this.selectedProfile);
    }
  }

  selectProfile(event){
    if(event !== "Any"){
      this.resultsArray = this.fuseSearch(event,["FLAVOR PROFILE"])
    } else
    if ((this.selectedForm === "Any" || this.selectedForm === null) && event === "Any" ){
      this.resultsArray = this.backupArray;
    } else
    if ((this.selectedForm !== "Any" || this.selectedForm !== null) && event === "Any" ){
      this.selectForm(this.selectedForm);
    }

  }

  fuseSearch(searchTerm, keyArray) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: keyArray  ///////////////////////////////
    };

    if (this.debug) {
      console.log("fuzzywuzzy", searchTerm);
    }
    var fuse = new Fuse(this.resultsArray, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchTerm);
    if (this.debug) {
      console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      el.item.matches = el.matches; // the matches matrix shows what actually matched during the search.  We're storing it inside of filteredSearchItems
      var matchedFoodArray = _.map(el.matches, function (le) {
        if (le.key == "menu_items.name") {
          return el.item.menu_items[le.arrayIndex];
        } //Here's we're using the array index for the matching items to look up the actual menu items.
      });
      if (matchedFoodArray[0] != undefined) {
        el.item.matching_food = matchedFoodArray; //the matching_food array is the one that will be displayed on the search screen results.
      }
      return el.item;
    });

    if (this.debug) {
      console.log("final array", arr);
    }
    return arr;
  } // end of fuse search

  gotoDescriptors(){
    this.navCtrl.push("FonaTestPage",{resultsArray: this.resultsArray})
  }

  showInfo($event,flavor){
    this.presentFlavorModal(flavor);
    event.stopPropagation();
  }

  presentFlavorModal(user) {
    const contactModal = this.modalCtrl.create("FlavorInfoPage",{flavorID: user});
    contactModal.present();
  }

}

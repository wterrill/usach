import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentTestPage } from './component-test';

@NgModule({
  declarations: [
    ComponentTestPage,
  ],
  imports: [
    IonicPageModule.forChild(ComponentTestPage),
  ],
})
export class ComponentTestPageModule {}

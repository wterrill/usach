import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QrStudentPage } from './qr-student';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  declarations: [
    QrStudentPage
  ],
  imports: [
    IonicPageModule.forChild(QrStudentPage),
    NgxQRCodeModule
  ],
})
export class QrStudentPageModule {}

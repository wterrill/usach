import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular"; // NavParams, 
import * as moment from "moment";
//import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { AlertController } from "ionic-angular";
import { ActionSheetController } from "ionic-angular";
//import { PopoverController } from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the QrStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-qr-student",
  templateUrl: "qr-student.html"
})
export class QrStudentPage {
  private amount = null;
  private user = null;
  private date = null;
  //private qrData = null;
  private createdCode = null;
  //private selectedDate = null;
  private period = null;
  private options = [
    "this week",
    "this and next week",
    "this month",
    "package"
  ];
  private costs = [20, 40, 60, 150];
  private cost = null;

  constructor(
    private navCtrl: NavController,
    //private navParams: NavParams,
    //private barcodescanner: BarcodeScanner,
    private alertCtrl: AlertController,
    private actionSheetCtrl: ActionSheetController,
    //private popoverCtrl: PopoverController,
    private auth: AuthenticationProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad QrStudentPage");
  }

  enterVal() {
    this.date = moment()
      .local()
      .toString();
    this.user = this.auth.currentUserData;
    this.presentConfirm();
  }

  presentConfirm() {
    //alert(JSON.stringify(this.user))
    let alertx = this.alertCtrl.create({
      title: "Confirmar Información de tu cuenta",
      message:
        "<p> &nbsp; </p><p class='confirme'>Usuario: " +
        this.user.displayName +
        "</p> <p>RUT: " +
        this.user.rut +
        "</p> <p>Banco: " +
        this.user.banco +
        "</p> <p>Tipo de Cuenta: " +
        this.user.tipo_cuenta +
        "</p> <p>Numero de cuenta: " +
        this.user.cuenta_Num +
        "</p> <p>Fecha: " +
        this.date + "</p>",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            this.amount = null;
          }
        },
        {
          text: "Crear",
          handler: () => {
            var temp = {
              displayName: this.user.displayName,
              uid: this.user.uid,
              photo: this.user.photoURL,
              email: this.user.email,
              rut: this.user.rut,
              tipo_cuenta: this.user.tipo_cuenta,
              cuenta_Num: this.user.cuenta_Num,
              banco: this.user.banco,
              date: this.date,
            };
            this.createdCode = JSON.stringify(temp);
          }
        }
      ],
      cssClass: "alertcss"
    });
    alertx.present();
  }

  selectPeriod(ev) {
    if (ev == this.options[0]) {
      alert(moment().endOf("week"));
      this.period = moment().endOf("week");
      this.cost = this.costs[this.options.indexOf(this.options[0])];
      alert(this.cost);
    }
    if (ev == this.options[1]) {
      alert(
        moment()
          .add("days", 7)
          .endOf("week")
      );
      this.period = moment()
        .add("days", 7)
        .endOf("week");
      this.cost = this.costs[this.options.indexOf(this.options[1])];
      alert(this.cost);
    }
    if (ev == this.options[2]) {
      alert(moment().endOf("month"));
      this.period = moment().endOf("month");
      this.cost = this.costs[this.options.indexOf(this.options[2])];
      alert(this.cost);
    }
    if (ev == this.options[3]) {
      alert(
        moment()
          .add("weeks", 7)
          .endOf("week")
      );
      this.period = moment()
        .add("weeks", 7)
        .endOf("week");
      this.cost = this.costs[this.options.indexOf(this.options[3])];
      alert(this.cost);
    }
  }

  done() {
    this.createdCode = null;
    this.navCtrl.push("LandingPage")
    
  }

  print(){
    this.presentPrint()
  }

  presentPrint() {
    //alert(JSON.stringify(this.user))
    let alertx = this.alertCtrl.create({
      title: "Confimar Impresión",
      message: "<p>El código fue enviado exitosamente a la impresora</p>",
      buttons: [
        {
          text: "Hecho",
          //role: "cancel"
        },
      ],
      cssClass: "alertcss"
    });
    alertx.present();
  }

      

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Modify your album",
      buttons: [
        {
          text: "Destructive",
          role: "destructive",
          handler: () => {
            console.log("Destructive clicked");
          }
        },
        {
          text: "Archive",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    actionSheet.present();
  }

  cheat(){
    //alert(JSON.stringify(this.user))
    let alertx = this.alertCtrl.create({
      title: "Recibiste una propina",
      message: "<p>Recibiste una propina de $205 pesos</p>",
      buttons: [
        {
          text: "Hecho",
          //role: "cancel"
        },
      ],
      cssClass: "alertcss"
    });
    alertx.present();
  }
}

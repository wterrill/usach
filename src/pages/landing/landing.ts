import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
import { AppVersion } from "@ionic-native/app-version";
import { AlertController } from "ionic-angular";
import { LoadingController } from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";


/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {
  private debug = false;
  private disable_button = false;
  private loading = null;
  private admin = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public appVersion: AppVersion,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private auth: AuthenticationProvider,
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
    if (this.auth.currentUserData !== null) {
      this.admin = true;
    }
  }

  ionViewDidLeave(){
    if(this.loading){
    this.loading.dismiss();
    }
  }

  gotoSend(){
    this.navCtrl.push("QrInstructorPage")
  }

  gotoReceive(){
    this.navCtrl.push("QrStudentPage")
  }

  // gotoSearch1(){
  //   this.navCtrl.push("SelectPage")
  // }

  gotoUpdate() {
    this.navCtrl.push("UpdateProfilePage");
  }

  // gotoSearch2(){
  //   this.loading = this.loadingCtrl.create({
  //     spinner: "ios",
  //     content: "Loading Flavors..."
  //   });
  //   this.loading.present();
  //   this.navCtrl.push("FonaTest2Page");
    
  // }

  gotoSlides(){
    this.navCtrl.push("SlideshowPage");
  }

  signOut() {
    var that = this;
    firebase
      .auth()
      .signOut()
      .then(function() {
        // Sign-out successful.
        //that.localuser = null;
        that.navCtrl.setRoot("LoginPage");
      })
      .catch(function(error) {
        // An error happened.
      });
  }

  // async showVersion() {
  //   let minVersion = null;
  //   let latestVersion = null;
  //   var videoArray = firebase.database().ref("AppInfo");
  //   videoArray.on("value", snapshot => {
  //     if (this.debug) {
  //       alert("snapshot: " + JSON.stringify(snapshot));
  //       alert("minVersion = " + snapshot.val().minVersion);
  //     }
  //     minVersion = snapshot.val().minVersion;
  //     latestVersion = snapshot.val().latestVersion;
  //   });

  //   let versionNumber = await this.appVersion.getVersionNumber(); 
  //   this.presentConfirm(
  //     versionNumber,
  //     minVersion,
  //     latestVersion
  //   );
  // }

  // async versionCheck() {
  //   let minVersion = null;
  //   let latestVersion = null;
  //   var videoArray = firebase.database().ref("AppInfo");
  //   videoArray.on("value", snapshot => {
  //     if (this.debug) {
  //       alert("snapshot: " + JSON.stringify(snapshot));
  //       alert("minVersion = " + snapshot.val().minVersion);
  //     }
  //     minVersion = snapshot.val().minVersion;
  //     latestVersion = snapshot.val().latestVersion;
  //   });
  //   let versionNumber = await this.appVersion.getVersionNumber(); //.then(x =>{ versionNumber = x
  //   let comparison_min = this.compareVersion(versionNumber, minVersion);
  //   //let comparison_latest = this.compareVersion(versionNumber, latestVersion)
    
  //   if(comparison_min === "less"){
  //     this.disable_button = true;
  //     this.presentOldVersion(versionNumber,minVersion);
  //   }
  // }

  // presentConfirm(versionNumber, minVersion, latestVersion) {
  //   let comparison_min = this.compareVersion(versionNumber, minVersion);
  //   let comparison_latest = this.compareVersion(versionNumber, latestVersion)

  //   let messagetxt =
  //     "Your version of FONA Friend is behind... but not overly so.  Please update when you have a chance.<br><br> Your version: " +
  //     versionNumber +
  //       "<br> Minimum required: " +
  //       minVersion +
  //       "<br>Latest version: " + latestVersion;

  //   if (comparison_latest === "equal") {
  //     messagetxt =
  //       "Your version of FONA Friend is up to date!<br><br> Your version: " +
  //       versionNumber +
  //       "<br> latest version: " +
  //       latestVersion;
  //   }

  //   if (comparison_min === "less") {
  //     messagetxt =
  //       "YOUR VERSION OF FONA FRIEND NEEDS TO BE UPDATED:<br><br> Your version: " +
  //       versionNumber +
  //       "<br> Minimum required: " +
  //       minVersion +
  //       "<br>Latest version: " + latestVersion;
  //   }
  //   let alertmsg = this.alertCtrl.create({
  //     title: name,
  //     message:
  //       messagetxt,
  //     buttons: [
  //       {
  //         text: "OK",
  //         role: "cancel",
  //         handler: () => {
  //           console.log("cancelled");
  //         }
  //       }
  //     ]
  //   });
  //   alertmsg.present();
  // }

  // compareVersion(a, b) {
  //   let a_arr = a.split(".", 3);
  //   let b_arr = b.split(".", 3);

  //   let a_int = a_arr.map(function(value) {
  //     let val = parseInt(value, 10);
  //     return val ? val : 0;
  //   });

  //   let b_int = b_arr.map(function(value) {
  //     let val = parseInt(value, 10);
  //     return val ? val : 0;
  //   });

  //   console.log(a_int);
  //   console.log(b_int);

  //   let status = "equal";
  //   let i = 0;

  //   do {
  //     if (a_int[i] < b_int[i]) status = "less";
  //     else if (a_int[i] > b_int[i]) status = "greater";
  //     i++;
  //   } while (status == "equal" && i < 3);

  //   return status;
  // }

  // presentOldVersion(versionNumber, minVersion) {

  //     let messagetxt =
  //       "YOUR VERSION OF FONA FRIEND NEEDS TO BE UPDATED BEFORE YOU CAN CONTINUE:<br><br> Your version: " +
  //       versionNumber +
  //       "<br> Minimum required: " +
  //       minVersion
    
  //   let alertmsg = this.alertCtrl.create({
  //     title: "YOU MUST UPDATE",
  //     message:
  //       messagetxt,
  //     buttons: [
  //       {
  //         text: "OK",
  //         role: "cancel",
  //         handler: () => {
  //           console.log("cancelled");
  //         }
  //       }
  //     ]
  //   });
  //   alertmsg.present();
  // }

  // showRequestAccess(){
  //   alert("please contact Jim Ellis at FONA International (Jellis@fona.com) to request access");
  // }

}

import { Component, ElementRef, ViewChild  } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular"; // NavParams
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Camera } from "@ionic-native/camera";
import * as firebase from "firebase";
import { Storage } from "@ionic/storage";
// import { SlideshowPage } from "../slideshow/slideshow";
// import { LandingPage } from "../landing/landing";
import { Platform } from "ionic-angular";
//import { ActionSheetController } from "ionic-angular";
import { LoadingController, AlertController } from "ionic-angular";



@IonicPage()
@Component({
  selector: "page-create-profile",
  templateUrl: "create-profile.html"
})
export class CreateProfilePage {
  private todo1: FormGroup;
  private firstName_var = null;
  private lastName_var = null;
  private phone_var: string = null;
  private birthday_var = null;
  private referral_var = null;
  private level_var = null;
  private bio_var = null;
  private base64Image: string = null;
  private enableIt: Boolean = false;
  private downloadURL = null;
  private uid = null;
  private loading = null;
  @ViewChild("myInput") myInput: ElementRef;

  // classes = {
  //   levels: [
  //     { name: "Beginner", id: "Beginner" },
  //     { name: "Intermediate", id: "Intermediate" },
  //     { name: "Advanced", id: "Advanced" }
  //   ]
  // };


  // api={"states":[{"name":"beer","id":"7"},
  // {"name":"women","id":"8"},
  // {"name":"song","id":"9"}]}

  constructor(
    private navCtrl: NavController,
    //private navParams: NavParams,
    private auth: AuthenticationProvider,
    private formBuilder: FormBuilder,
    private camera: Camera,
    private storage: Storage,
    private platform: Platform,
    // private actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
    this.todo1 = this.formBuilder.group({
      firstName_form: ["", Validators.required],
      lastName_form: ["", Validators.required],
      phone_form: ["", Validators.required],
      // birthday_form: ["", Validators.pattern('.')],
      // referral_form: ["", Validators.required],
      // class_form: ["", Validators.required]
    });

    //For debugging stuff... see if we're running on web:
    if (this.platform.is("cordova")) {
      // This will only print when running on mobile
      console.log("I'm a regular browser!");
    } else {
      //we're running on desktop, so let's take care of the camera stuff
      this.enableIt = true;
      this.base64Image = 
        "data:image/gif;base64,R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
    }

    //get uic from auth:
    if (this.auth.currentUser) {
      this.uid = this.auth.currentUser.uid;
    } else {
      this.uid = 3;
    }

    this.loading = this.loadingCtrl.create({
      spinner: "ios",
      content: "Creating profile. Please Wait..."
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CreateProfilePage");
  }

  resize() {
    this.myInput.nativeElement.style.height =
      this.myInput.nativeElement.scrollHeight + "px";
  }

  selectState(ev) {
    this.level_var = ev;
  }

  dateChanged(ev){
    this.birthday_var = ev;
  }

  accessGallery() {
    this.camera
      .getPicture({
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 600
      })
      .then(
        imageData => {
          this.base64Image = "data:image/jpeg;base64," + imageData;
          this.enableIt = true;
        },
        err => {
          console.log(err);
        }
      );
  }

  // takeAPic() {
  //   var options = {
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE,
  //     allowEdit:true,
  //     correctOrientation: true,
  //     targetWidth: 600
  //     //targetHeight: 1000
  //   };

  //   this.camera.getPicture(options).then(
  //     imageData => {
  //       // imageData is either a base64 encoded string or a file URI
  //       // If it's base64:
  //       this.base64Image = "data:image/jpeg;base64," + imageData;
  //       console.log(this.base64Image);
  //       this.enableIt = true;
  //     },
  //     err => {
  //       // Handle error
  //     }
  //   );
  // }

  submitted() {
    console.log("submitted");
    this.loading.present();
    //this.uploadPic();
    this.updateAuthUser();
  }

  // uploadPic() {
  //   let that = this;
  //   let imagefile = this.base64Image.split(",")[1]; //this gets rid of the "data:image/jpeg;base64," part of the file
  //   console.log("uploadPic2");

  //   let uploadTask = firebase
  //     .storage()
  //     .ref("users/"+this.uid)
  //     .child("profile_pic")
  //     .putString(imagefile, "base64", { contentType: "image/jpg" });
  //   uploadTask.on(
  //     firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
  //     function(snapshot: any) {
  //       // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
  //       var progress = snapshot.bytesTransferred / snapshot.totalBytes * 100;
  //       console.log("Upload is " + progress + "% done");
  //       switch (snapshot.state) {
  //         case firebase.storage.TaskState.PAUSED: // or 'paused'
  //           console.log("Upload is paused");
  //           break;
  //         case firebase.storage.TaskState.RUNNING: // or 'running'
  //           console.log("Upload is running");
  //           break;
  //       }
  //     },
  //     function(error) {
  //       alert("error:" + JSON.stringify(error));
  //     },
  //     function() {
  //       // Upload completed successfully, now we can get the download URL
  //       console.log("got to downloadURL");
  //       that.downloadURL = uploadTask.snapshot.downloadURL;
  //       // that.updateAuthUser();
  //     }
  //   );
  // }

  updateAuthUser() {
    let that = this;
    console.log("got to updateAuthUser()");
    var user = firebase.auth().currentUser;
    console.log("user:" + JSON.stringify(user));

    user
      .updateProfile({
        displayName: this.firstName_var + " " + this.lastName_var,
        photoURL: "https://firebasestorage.googleapis.com/v0/b/usach-5ed61.appspot.com/o/Nix.jpg?alt=media&token=e7eb5b61-611b-4096-ac7f-169125b1aed8r"
      })
      .then(function() {
        console.log("auth profile successfully updated");
        that.updateDatabase();
      })
      .catch(function(error) {
        console.log("auth profile update failed");
      });
  }

  updateDatabase() {
    let array = [ "1stRoutine" ]
    firebase
      .database()
      .ref("users/" + this.uid)
      .set({
        displayName: this.firstName_var + " " + this.lastName_var,
        firstName: this.firstName_var,
        lastName: this.lastName_var,
        // birthday: this.birthday_var,
        // referral: this.referral_var,
        telephone: this.phone_var,
        // profile_pic: this.downloadURL,
        // class_level: this.level_var,
        // bio: this.bio_var,
        uid: this.uid,
        permissions: "student",
        // currentClass: {name:"None", shortName:"None", shortNameFull:"None"}
      });

      // firebase
      // .database()
      // .ref("users/" + this.uid + "/videoPermissions/")
      // .set({
      //   array
      // });

      
    this.loading.dismiss();
    console.log("Profile successfully updated");
    this.navigateOut();

  }

  navigateOut() {
    this.storage.get("show_slideshow").then(val => {
      if (val==true || val==undefined) {
        //and they'll have to set up a profile
        this.navCtrl.setRoot("SlideshowPage");  
      } else {
        this.navCtrl.setRoot("LandingPage");
      }
    }); //end this.storage.get()
  }

  // presentPhotoWarning(source) {
  //   let messagetxt = null;
  //   if (source==="camera"){
  //     messagetxt = "Since we're all trying to get to know each other, would it be possible to take a picture that shows your face?  I know you have the cutest pet in the world, but it would be great nice to put the name with the face!";
  //   } else {
  //     messagetxt = "Since we're all trying to get to know each other, would it be possible to select a picture that shows your face?  I know you have the cutest pet in the world, but it would be great to put the name with the face!";
  //   } 
  //   let alert = this.alertCtrl.create({
  //     title: "A favor...",
  //     message: messagetxt,
  //     buttons: [
  //       {
  //         text: "Ok",
  //         role: "cancel",
  //         handler: () => {
  //           if(source ==="camera"){
  //             this.takeAPic();
  //           } else {
  //             this.accessGallery();
  //           }
  //         }
  //       }
  //     ]
  //   });
  //   alert.present();
  // }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalVideoPage } from './local-video';

@NgModule({
  declarations: [
    LocalVideoPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalVideoPage),
  ],
})
export class LocalVideoPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoPickerPage } from './video-picker';

@NgModule({
  declarations: [
    VideoPickerPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoPickerPage),
  ],
})
export class VideoPickerPageModule {}

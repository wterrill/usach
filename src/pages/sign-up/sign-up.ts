import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import * as firebase from "firebase";
import { AlertController } from 'ionic-angular';
//import { LoginPage } from '../login/login'

//https://firebase.google.com/docs/auth/web/start?authuser=0

@IonicPage()
@Component({
  selector: "page-sign-up",
  templateUrl: "sign-up.html"
})
export class SignUpPage {
  private todo: FormGroup;
  private email_var: string = null;
  private password_var: string = null;
  private password2_var: string = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController     
  ) {
    this.todo = this.formBuilder.group({
      email_form: ["", Validators.compose([Validators.required, Validators.email, Validators.pattern("^(?=.*\.)(?=.*@).+")  ])],
      password_form: ["", Validators.compose([Validators.required, Validators.minLength(6)])],
      password2_form: ["", Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }

  createUser(email, password) {
    var that = this;
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(user => {

        this.navCtrl.push("LoginPage");  //<<<<<< ADDED FOR CONFIRMATION EMAIL STUFF
        // if (user && user.emailVerified === false) {
        //   user.sendEmailVerification().then(function() {     CONFIRMATION EMAIL
        //     that.presentInfo();
        //   });
        // }
      })
      .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        alert("createUserError:" + errorCode + ":-:" + errorMessage);
        // ...
      });
  }

  submitted(todoValues) {
    console.log(todoValues);
    if(this.password_var == this.password2_var)
    {
      this.createUser(this.email_var, this.password_var);
    } else {
      this.presentPasswordMatch();
    }
  }

  presentInfo() {
    let alert = this.alertCtrl.create({
      title: 'Confirmation Email',
      message: `A confirmation Email has been sent to your email address.<br> 
                Please continue the registration process by visiting your
                email account, and confirming the address. <br> After the email address is confirmed, sign into the app. `,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            this.navCtrl.push("LoginPage");
          }
        }
      ]
    });
    alert.present();
  }

  presentPasswordMatch() {
    let alert = this.alertCtrl.create({
      title: "Passwords don't match",
      message: `The Passwords you entered do not match.  Please try again.`,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            this.password_var = null;
            this.password2_var = null;
          }
        }
      ]
    });
    alert.present();
  }

  
}

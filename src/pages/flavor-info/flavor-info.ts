import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the FlavorInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-flavor-info',
  templateUrl: 'flavor-info.html',
})
export class FlavorInfoPage {
  private flavor = null;
  private highlight = null

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController
  ) {
    this.flavor = navParams.get('flavorID')
    this.highlight = this.flavor["FLAVOR PROFILE"]

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
  }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

}

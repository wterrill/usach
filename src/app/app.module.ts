import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';  //Popover
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { VideoPlayer } from '@ionic-native/video-player';
import * as firebase from 'firebase';
import { File } from '@ionic-native/file'
import { FileTransfer } from '@ionic-native/file-transfer';
import { IonicStorageModule } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { Camera } from '@ionic-native/camera';  //CameraOptions
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppVersion } from '@ionic-native/app-version';

import { PopoverComponent} from '../components/popover/popover';


//for lazy loading
import { HttpModule } from '@angular/http';

  // Initialize Firebase
  export const config = {
    apiKey: "AIzaSyBIyVB7bUtfPOTSziSMLiVQB2nFALXmrCg",
    authDomain: "usach-5ed61.firebaseapp.com",
    databaseURL: "https://usach-5ed61.firebaseio.com",
    projectId: "usach-5ed61",
    storageBucket: "",
    messagingSenderId: "702748828932"
  };
  firebase.initializeApp(config);
  

@NgModule({
  declarations: [
    MyApp,
    PopoverComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    YoutubeVideoPlayer,
    VideoPlayer,
    File,
    FileTransfer,
    BarcodeScanner,
    AuthenticationProvider,
    Camera,
    ScreenOrientation,
    AppVersion
  ]
})
export class AppModule {}
